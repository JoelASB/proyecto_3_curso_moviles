using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Object_Pool_Enemies : MonoBehaviour
{
    [Header("Pool Enemies")]
    public List<GameObject> Enemies;
    public GameObject prefab_Enemie;

    [Header("Max Enemies in scene")]
    public int N_Enemies;
    // Start is called before the first frame update
    void Start() {
        InstantEnemies();
        InvokeRepeating("getEnemie",1f,Random.Range(2f, 5f));
    }
    public void InstantEnemies() {
        GameObject tmp;
        for (int i = 0; i < N_Enemies; i++) {
            tmp = Instantiate(prefab_Enemie, transform.position, transform.rotation);
            Enemies.Add(tmp);
            tmp.transform.SetParent(this.transform);
            tmp.SetActive(false);
        }
    }
    public void getEnemie() {
        if (Enemies.Count > 0) {
            GameObject tmp = Enemies[0];
            tmp.transform.position = new Vector3(Random.Range(-3,3),tmp.transform.position.y, 204.4f);
            Enemies.Remove(tmp);
            tmp.SetActive(true);
        }
    }
    //public void setEnemie(GameObject e) {
    //    Enemies.Add(e);
    //}
    private void OnTriggerEnter(Collider other) {
        Debug.Log("dsfsdfsdfdsfsfsdfsdf");
        Enemies.Add(other.gameObject);
        other.gameObject.SetActive(false);
    }
}
