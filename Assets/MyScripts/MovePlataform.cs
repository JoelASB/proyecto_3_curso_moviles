using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlataform : MonoBehaviour
{
    public float speed;
    public Vector3 TeleportPoint;
    public Vector3 NewPoint;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (this.transform.position.z <= TeleportPoint.z)
            this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, NewPoint.z);
        this.transform.position -= Vector3.forward * speed * Time.deltaTime;
    }
    private void OnDrawGizmos() {
        Vector2 _transform = new Vector2(this.transform.position.x, this.transform.position.z);
        Gizmos.color = Color.red;
        
        // Verticales
        Gizmos.DrawLine(new Vector3(_transform.x + 5, 1, _transform.y - 14.4f),
                        new Vector3(_transform.x + 5, 1, _transform.y + 35.6f));
        Gizmos.DrawLine(new Vector3(_transform.x - 5, 1, _transform.y - 14.4f),
                        new Vector3(_transform.x - 5, 1, _transform.y + 35.6f));

        // Horizontales
        Gizmos.DrawLine(new Vector3(_transform.x - 5, 1, _transform.y - 14.4f),
                        new Vector3(_transform.x + 5, 1, _transform.y - 14.4f));
        Gizmos.DrawLine(new Vector3(_transform.x - 5, 1, _transform.y + 35.6f),
                        new Vector3(_transform.x + 5, 1, _transform.y + 35.6f));
    }
}
