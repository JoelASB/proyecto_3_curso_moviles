﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Automovil : MonoBehaviour
{
    [Header("Movility automovil")]
    public float Speed;
    public int direction;

    [Header("Buttons")]
    public Button ModeControls;
    public Button_Push Left;
    public Button_Push Right;

    [Header("Change buttons to Acelerometro")]
    public bool ActiveButtons;

    private void Start() {
        ChangeControl();
        ModeControls.onClick.AddListener(()=> ChangeControl());
    }
    // Update is called once per frame
    void Update() {
        if (!ActiveButtons) {
            if (Input.acceleration.x < -0.05)
            {
                direction = -1;
            }
            else if (Input.acceleration.x > 0.05)
            {
                direction = 1;
            }
            else if (Input.acceleration.x > -0.05 && Input.acceleration.x < 0.05)
            {
                direction = 0;
            }
        } else {
            
            if(Left.isPressed == true && Right.isPressed == false) {
                direction = -1;
            } else if (Right.isPressed == true && Left.isPressed == false) {
                direction = 1;
            } else {
                direction = 0;
            }
        
        }
        transform.position = new Vector3(transform.position.x + Speed * direction * Time.deltaTime,
                                            transform.position.y, transform.position.z);
    }
    void ChangeControl() {
        ActiveButtons = !ActiveButtons;
        if (!ActiveButtons) {
            Left.gameObject.SetActive(false);
            Right.gameObject.SetActive(false);
        } else {
            Left.gameObject.SetActive(true);
            Right.gameObject.SetActive(true);
        }
    }
}
