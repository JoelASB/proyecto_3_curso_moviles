using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Vehicle : MonoBehaviour
{
    public float speed;
    // Start is called before the first frame update
    void Update() {
        this.transform.position -= Vector3.forward * speed * Time.deltaTime;
    }


}
