﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Button_Push : MonoBehaviour, IPointerUpHandler, IPointerDownHandler
{
    public bool isPressed;

    void IPointerUpHandler.OnPointerUp(PointerEventData EventUp) {
        isPressed = false;
    }
    void IPointerDownHandler.OnPointerDown(PointerEventData EventDown) {
        isPressed = true;
    }
}
